﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace ReaderXml
{

    public class Account
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
    }

    // В классе реализаторе сохранять данные в какой-нибудь файл. Формат на ваше усмотрение - json, xml, csv, etc
    public interface IRepository<T>
    {
        // когда реализуете этот метод, используйте yield (его можно использовать просто в методе, без создания отдельного класса)
        IEnumerable<T> GetAll();
        T GetOne(Func<T, bool> predicate);
        void Add(T item);
    }

    interface IAccountService
    {
        // В классе-реализаторе делать валидацию: проверить что имена не пустые, что возраст > 18 лет, можете также добавить свои правила
        // Если валидация прошла успешно, то добавлять аккаунт в репозиторий
        void AddAccount(Account account);
    }

    class AccountService : IRepository<Account>, IAccountService
    {
        XmlSerializer xml = new XmlSerializer(typeof(List<Account>));
        string path = "User.xml";

        public void Add(Account item)
        {
            List<Account> accounts = new List<Account>();
            accounts.Add(item);

            using (var file = new FileStream(path, FileMode.OpenOrCreate))
            {
                xml.Serialize(file, accounts);
            }
        }

        public void AddAccount(Account account)
        {
            if (string.IsNullOrWhiteSpace(account.FirstName) ||  string.IsNullOrWhiteSpace(account.LastName))
                throw new ArgumentException("Параметры заполнены не верно.");

            if (GetAge(account.BirthDate) <= 18)
                throw new ArgumentException("Возраст должен быть больше 18 лет.");

            using (var file = new FileStream(path, FileMode.OpenOrCreate))
            {
               xml.Serialize(file, account);
            }

        }

        public IEnumerable<Account> GetAll()
        {
            using (var file = new FileStream(path, FileMode.OpenOrCreate))
            {
                var nextLine = xml.Deserialize(file) as List<Account>;

                for (int i = 0; i < nextLine.Count; i++)
                {
                    yield return nextLine[i];
                }
            }
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            using (var file = new FileStream(path, FileMode.OpenOrCreate))
            {
                var user = xml.Deserialize(file) as List<Account>;
              
                for (int i = 0; i < user.Count; i++)
                {
                    if (predicate(user[i]))
                    {
                        return user[i];
                    }
                }

                return new Account {FirstName = "", LastName = "", BirthDate = new DateTime(1994, 01, 01)};
            }
        }

        private static int GetAge(DateTime birthDate)
        {
            var now = DateTime.Today;
            return now.Year - birthDate.Year - 1 +
                ((now.Month > birthDate.Month || now.Month == birthDate.Month && now.Day >= birthDate.Day) ? 1 : 0);
        }

    }
}
