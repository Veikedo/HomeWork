﻿using System;

namespace Matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix A = new Matrix(5, 5);
            Matrix B = new Matrix(5, 5);

            A.Fill();
            B.Fill();

            WriteText("Ввод Матрица А: ");
            Console.WriteLine(A.ToString());

            WriteText("Ввод Матрица B: ");
            Console.WriteLine(B.ToString());

            Matrix C = A - B;
            Matrix D = A + B;

            WriteText("Вычитание матриц А и B: ");
            Console.WriteLine(C.ToString());

            WriteText("Сложение матриц А и B: ");
            Console.WriteLine(D.ToString());

            WriteText("Замена элементов в матрице\"A\":");
            A[0, 0] = -777;
            Console.WriteLine(A.ToString());

            WriteText("Получение элементов матрицы\"A\":");
            Console.WriteLine(A[1, 1]);

            Console.ReadKey();
        }

        static void WriteText(string text)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
